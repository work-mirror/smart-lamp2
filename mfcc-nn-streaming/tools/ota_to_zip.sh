#!/bin/bash

set -e
set -u

if [ "$#" -ne 3 ]; then
  echo "Illegal number of parameters"
  exit 1
fi

readonly DEVICE=$1
readonly HW_VERSION=$2
readonly SW_VERSION=$3
readonly TMP_DIR=$(mktemp -d)/firmware/${DEVICE}/hw-v0.0.${HW_VERSION}
readonly TMP_JSON=${TMP_DIR}/info.json
readonly OTA_BIN_MD5=`md5sum build/kws_lamp7.bin | awk '{print $1}'`
readonly ZIP_TARGET=`pwd`/ota.${DEVICE}.${HW_VERSION}.${SW_VERSION}.zip

rm -f ota.${DEVICE}.*.zip

mkdir -p ${TMP_DIR}
cp build/kws_lamp7.bin ${TMP_DIR}/${OTA_BIN_MD5}.bin

echo '{' > ${TMP_JSON}
echo " \"version\":${SW_VERSION}," >> ${TMP_JSON}
echo ' "interval":3600,' >> ${TMP_JSON}
echo " \"path\":\"/firmware/${DEVICE}/hw-v0.0.${HW_VERSION}/${OTA_BIN_MD5}.bin\"," >> ${TMP_JSON}
echo " \"checksum\":\"${OTA_BIN_MD5}\"" >> ${TMP_JSON}
echo '}' >> ${TMP_JSON}

cd ${TMP_DIR}/../../../ && zip -r ${ZIP_TARGET} .