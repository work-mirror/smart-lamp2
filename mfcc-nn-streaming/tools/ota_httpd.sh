#!/bin/bash

set -e
set -u

if [ "$#" -ne 3 ]; then
  echo "Illegal number of parameters"
  exit 1
fi

readonly DEVICE=$1
readonly HW_VERSION=$2
readonly SW_VERSION=$3
readonly TMP_DIR=$(mktemp -d)
readonly ZIP_TARGET=`pwd`/ota.${DEVICE}.${HW_VERSION}.${SW_VERSION}.zip

unzip ${ZIP_TARGET} -d ${TMP_DIR}

echo "ota `ifconfig | \
      grep -Eo 'inet (addr:)?([0-9]*\.){3}[0-9]*' | \
      grep -Eo '192\.([0-9]*\.){2}[0-9]*'`:8080"
busybox httpd -f -h ${TMP_DIR} -p 8080