/******************************************************************************
 * @copyright Copyright (c) A1 Company LLC. All rights reserved.
 *****************************************************************************/

#include "non_volatile_storage.h"
#include "nvs.h"
#include "nvs_flash.h"
#include "esp_log.h"

#define SIZEOF_KEY 10

/*****************************************************************************/

static const char *TAG = "nvs";

static esp_err_t nvs_try_get_integer(nvs_value_id_t id, int32_t *out);
static esp_err_t nvs_try_get_string(nvs_value_id_t id, char *str, size_t *len);

/*****************************************************************************/

static void maybe_set_integer_to_default(nvs_value_id_t id, int32_t value)
{
  int32_t prev;
  if(nvs_try_get_integer(id, &prev) != ESP_OK)
  {
    nvs_set_integer(id, value);
    ESP_LOGW(TAG, "setting nvs id %x to default value %u", id, value);
  }
}

/*****************************************************************************/

static void nvs_set_defaults_on_first_boot_or_after_update(
  const struct nvs_device_defaults * const defaults)
{
  //set specific default parameters for current device
  for(int i = 0; i < defaults->integers.length; i++)
  {
    nvs_value_id_t id = defaults->integers.arr[i].id;
    int32_t value = defaults->integers.arr[i].value;
    maybe_set_integer_to_default(id, value);
  }
}

/*****************************************************************************/

void nvs_init(const struct nvs_device_defaults * const defaults)
{
  esp_err_t err = nvs_flash_init();
  if(err != ESP_OK)
  {
    if(err == ESP_ERR_NVS_NO_FREE_PAGES || err == ESP_ERR_NVS_NEW_VERSION_FOUND)
    {
      ESP_LOGW(TAG, "no free pages: erase flash");
      ESP_ERROR_CHECK(nvs_flash_erase()); //nvs partition was truncated and needs to be erased
      ESP_LOGI(TAG, "reinitialize flash");
      err = nvs_flash_init();  //retry nvs flash init
    }
    else
    {
      ESP_LOGE(TAG, "%s", esp_err_to_name(err));
    }
  }
  ESP_ERROR_CHECK(err);
  nvs_set_defaults_on_first_boot_or_after_update(defaults);
}

/*****************************************************************************/

void nvs_set_integer(nvs_value_id_t id, int32_t value)
{
  nvs_handle handle = NULL;
  char key[SIZEOF_KEY] = {'\0'};

  //convert id to string
  assert(snprintf(key, sizeof(key) - 1, "%x", id));

  //save integer into nvs flash
  ESP_ERROR_CHECK(nvs_open("value:integer", NVS_READWRITE, &handle));
  ESP_ERROR_CHECK(nvs_set_i32(handle, key, value));
  ESP_ERROR_CHECK(nvs_commit(handle));
  nvs_close(handle);
}

/*****************************************************************************/

static esp_err_t nvs_try_get_integer(nvs_value_id_t id, int32_t *out)
{
  nvs_handle handle = NULL;
  char key[SIZEOF_KEY] = {'\0'};

  //convert id to string
  assert(snprintf(key, sizeof(key) - 1, "%x", id));

  //get integer from nvs flash
  esp_err_t err = nvs_open("value:integer", NVS_READONLY, &handle);
  if(err == ESP_OK)
  {
    err = nvs_get_i32(handle, key, out);
    nvs_close(handle);
  }

  return err;
}

/*****************************************************************************/

int32_t nvs_get_integer(nvs_value_id_t id)
{
  int32_t value = 0;
  ESP_ERROR_CHECK(nvs_try_get_integer(id, &value));
  return value;
}

/*****************************************************************************/

void nvs_set_float(nvs_value_id_t id, float value)
{
  nvs_handle handle = NULL;
  char key[SIZEOF_KEY] = {'\0'};

  //convert id to string
  assert(snprintf(key, sizeof(key) - 1, "%x", id));

  //save float into nvs flash
  ESP_ERROR_CHECK(nvs_open("value:float", NVS_READWRITE, &handle));
  ESP_ERROR_CHECK(nvs_set_blob(handle, key, (void *)&value, sizeof(value)));
  ESP_ERROR_CHECK(nvs_commit(handle));
  nvs_close(handle);
}

/*****************************************************************************/

float nvs_get_float(nvs_value_id_t id)
{
  nvs_handle handle = NULL;
  char key[SIZEOF_KEY] = {'\0'};
  float value = 0.0;
  size_t size = sizeof(value);

  //convert id to string
  assert(snprintf(key, sizeof(key) - 1, "%x", id));

  //get float from nvs flash
  ESP_ERROR_CHECK(nvs_open("value:float", NVS_READONLY, &handle));
  ESP_ERROR_CHECK(nvs_get_blob(handle, key, (void *)&value, &size));
  nvs_close(handle);

  return value;
}

/*****************************************************************************/

void nvs_set_string(nvs_value_id_t id, const char *str)
{
  nvs_handle handle = NULL;
  char key[SIZEOF_KEY] = {'\0'};

  //convert id to string
  assert(snprintf(key, sizeof(key) - 1, "%x", id));

  //save string into nvs flash
  ESP_ERROR_CHECK(nvs_open("value:string", NVS_READWRITE, &handle));
  ESP_ERROR_CHECK(nvs_set_str(handle, key, str));
  ESP_ERROR_CHECK(nvs_commit(handle));
  nvs_close(handle);
}

/*****************************************************************************/

static esp_err_t nvs_try_get_string(nvs_value_id_t id, char *str, size_t *len)
{
  nvs_handle handle = NULL;
  char key[SIZEOF_KEY] = {'\0'};

  //convert id to string
  assert(snprintf(key, sizeof(key) - 1, "%x", id));

  //get string from nvs flash
  esp_err_t err = nvs_open("value:string", NVS_READONLY, &handle);
  if(err == ESP_OK)
  {
    err = nvs_get_str(handle, key, str, len);
    nvs_close(handle);
  }

  return err;
}

/*****************************************************************************/

size_t nvs_get_string(nvs_value_id_t id, char *str, size_t len)
{
  esp_err_t err = nvs_try_get_string(id, str, &len);
  if(err == ESP_ERR_NVS_INVALID_LENGTH)
  {
    ESP_LOGE(TAG, "nvs_get_string: ESP_ERR_NVS_INVALID_LENGTH");
  }
  else
  {
    ESP_ERROR_CHECK(err);
  }
  return len;
}

/*****************************************************************************/

void nvs_set_array(nvs_value_id_t id, const char array[], size_t size)
{
  nvs_handle handle = NULL;
  char key[SIZEOF_KEY] = {'\0'};

  //convert id to string
  assert(snprintf(key, sizeof(key) - 1, "%x", id));

  //save array into nvs flash
  ESP_ERROR_CHECK(nvs_open("value:array", NVS_READWRITE, &handle));
  ESP_ERROR_CHECK(nvs_set_blob(handle, key, array, size));
  ESP_ERROR_CHECK(nvs_commit(handle));
  nvs_close(handle);
}

/*****************************************************************************/

void nvs_get_array(nvs_value_id_t id, char array[], size_t size)
{
  nvs_handle handle = NULL;
  char key[SIZEOF_KEY] = {'\0'};

  //convert id to string
  assert(snprintf(key, sizeof(key) - 1, "%x", id));

  //get array from nvs flash
  ESP_ERROR_CHECK(nvs_open("value:array", NVS_READONLY, &handle));
  ESP_ERROR_CHECK(nvs_get_blob(handle, key, array, &size));
  nvs_close(handle);
}

/*****************************************************************************/
