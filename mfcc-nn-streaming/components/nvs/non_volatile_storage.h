/******************************************************************************
 * @copyright Copyright (c) A1 Company LLC. All rights reserved.
 *****************************************************************************/

#pragma once /****************************************************************/

#include <stdint.h>
#include <stddef.h>

typedef enum          /* $ openssl rand -hex 4 */
{
  NVS_INT_USER_BUTTON_PRESS_COUNTER = 0xbdd8c371,
  NVS_INT_LAMP_ONOFF                = 0x16d06e30,
  NVS_INT_LAMP_BRIGHTNESS           = 0x4a49bce8,
  NVS_INT_LAMP_COLOR_TEMPERATURE    = 0xad058964,

} nvs_value_id_t;

struct nvs_device_defaults
{
  struct
  {
    const struct
    {
      nvs_value_id_t id;
      int32_t        value;
    } *arr;
    size_t length;
  } integers;
};

void nvs_init(const struct nvs_device_defaults * const defaults);

void nvs_set_integer(nvs_value_id_t id, int32_t value);
int32_t nvs_get_integer(nvs_value_id_t id);

void nvs_set_float(nvs_value_id_t id, float value);
float nvs_get_float(nvs_value_id_t id);

void nvs_set_string(nvs_value_id_t id, const char *str);
size_t nvs_get_string(nvs_value_id_t id, char *str, size_t len);

void nvs_set_array(nvs_value_id_t id, const char array[], size_t size);
void nvs_get_array(nvs_value_id_t id, char array[], size_t size);

/*****************************************************************************/
