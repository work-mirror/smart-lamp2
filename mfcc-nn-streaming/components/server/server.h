/******************************************************************************
 * @copyright Copyright (c) Link Line Ukraine Ltd. All rights reserved.
 *****************************************************************************/

#pragma once /****************************************************************/

struct http_server_context
{
    void (*on_set_brightness)(uint8_t);
    void (*on_set_balance)(uint8_t);
    void (*on_set_wifi_credentials)(uint8_t*, uint8_t*);
};

void http_server_start(struct http_server_context *context);

/*****************************************************************************/
