###############################################################################
# @copyright Copyright (c) A1 Company LLC. All rights reserved.
###############################################################################

CFLAGS += -Werror

COMPONENT_ADD_INCLUDEDIRS := .

COMPONENT_EMBED_FILES := assets/favicon.png assets/index.html \
                         assets/updated.html \
                         assets/swagger.json \
                         assets/swagger-ui.html.gz

COMPONENT_EMBED_TXTFILES := assets/error.html

###############################################################################
