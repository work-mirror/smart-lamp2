/******************************************************************************
 * @copyright Copyright (c) Link Line Ukraine Ltd. All rights reserved.
 *****************************************************************************/

#include <esp_event.h>
#include <esp_log.h>
#include <esp_system.h>
#include <nvs_flash.h>
#include <sys/param.h>
#include <freertos/timers.h>

#include <esp_http_server.h>
#include <esp_ota_ops.h>

#include "multipartparser.h"
#include "cJSON.h"
#include "server.h"

/*****************************************************************************/

extern const uint8_t favicon_png_start[]      asm("_binary_favicon_png_start");
extern const uint8_t favicon_png_end[]        asm("_binary_favicon_png_end");

extern const uint8_t index_html_start[]       asm("_binary_index_html_start");
extern const uint8_t index_html_end[]         asm("_binary_index_html_end");

extern const uint8_t updated_html_start[]     asm("_binary_updated_html_start");
extern const uint8_t updated_html_end[]       asm("_binary_updated_html_end");

extern const uint8_t swagger_json_start[]     asm("_binary_swagger_json_start");
extern const uint8_t swagger_json_end[]       asm("_binary_swagger_json_end");

extern const uint8_t swagger_html_gz_start[]  asm("_binary_swagger_ui_html_gz_start");
extern const uint8_t swagger_html_gz_end[]    asm("_binary_swagger_ui_html_gz_end");

extern const uint8_t error_html_start[]       asm("_binary_error_html_start");

/*****************************************************************************/

static const char *TAG = "server";

static struct http_server_context g_callback;

/*****************************************************************************/

struct parser_context
{
  enum { P_STATE_INIT, P_STATE_PART, P_STATE_HEADER, P_STATE_BIN_DATA } state;
  const esp_partition_t *partition;
  esp_ota_handle_t update_handle;
  char* err;
};

static struct parser_context* parser_get_context(multipartparser* parser)
{
  return (struct parser_context*)parser->data;
}

/*---------------------------------------------------------------------------*/

static void parser_set_context(multipartparser* parser, struct parser_context* context)
{
  parser->data = context;
}

/*---------------------------------------------------------------------------*/

static int on_part_begin(multipartparser* parser)
{
  ESP_LOGD(TAG, "on_part_begin");
  if(parser_get_context(parser)->state == P_STATE_INIT)
  {
    parser_get_context(parser)->state = P_STATE_PART;
  }
  return 0;
}

/*---------------------------------------------------------------------------*/

static int on_header_field(multipartparser* parser, const char* data, size_t size)
{
  ESP_LOGD(TAG, "on_header_field: %.*s", size, data);
  if(parser_get_context(parser)->state == P_STATE_PART && size == 19 && !strncasecmp("Content-Disposition", data, size))
  {
    parser_get_context(parser)->state = P_STATE_HEADER;
  }
  return 0;
}

/*---------------------------------------------------------------------------*/

static int on_header_value(multipartparser* parser, const char* data, size_t size)
{
  ESP_LOGI(TAG, "on_header_value: %.*s", size, data);
  if(parser_get_context(parser)->state == P_STATE_HEADER)
  {
    if(size >= 28 && !strncasecmp("form-data; name=\"fileupload\"", data, 28))
    {
      parser_get_context(parser)->state = P_STATE_BIN_DATA;
    }
  }
  return 0;
}

/*---------------------------------------------------------------------------*/

static int on_data(multipartparser* parser, const char* data, size_t size)
{
  ESP_LOGD(TAG, "on_data: %.*s", size, data);
  if(size)
  {
    if(parser_get_context(parser)->state == P_STATE_BIN_DATA)
    {
      const esp_partition_t *partition = parser_get_context(parser)->partition;

      if(!partition)
      {
        const esp_partition_t **update_partition = &parser_get_context(parser)->partition;
        const esp_partition_t *configured_partition;
        const esp_partition_t *running_partition;
        configured_partition = esp_ota_get_boot_partition();
        running_partition = esp_ota_get_running_partition();

        ESP_LOGI(TAG, "Configured OTA boot partition at offset 0x%08x", configured_partition->address);
        ESP_LOGI(TAG, "Running partition type %d subtype %d (offset 0x%08x)",
                 running_partition->type, running_partition->subtype, running_partition->address);

        *update_partition = esp_ota_get_next_update_partition(configured_partition);
        ESP_LOGI(TAG, "Writing to partition subtype %d at offset 0x%x",
                 (*update_partition)->subtype, (*update_partition)->address);
        if(*update_partition == NULL)
        {
          ESP_LOGE(TAG, "invalid ota: %.*s", size, data);
          parser_get_context(parser)->err = "esp_ota_get_partition failed";
          return 0;
        }
        partition = parser_get_context(parser)->partition;
      }

      if(partition)
      {
        esp_ota_handle_t* update_handle = &parser_get_context(parser)->update_handle;
        if(!*update_handle)
        {
          esp_err_t err = esp_ota_begin(partition, OTA_SIZE_UNKNOWN, update_handle);
          if(err != ESP_OK) {
            ESP_LOGE(TAG, "esp_ota_begin failed (%s)", esp_err_to_name(err));
            parser_get_context(parser)->err = "esp_ota_begin failed";
            return 0;
          }
          ESP_LOGI(TAG, "esp_ota_begin succeeded");
        }

        esp_err_t err = esp_ota_write(*update_handle, (const void *)data, size);
        if(err != ESP_OK) {
          ESP_LOGE(TAG, "esp_ota_write failed (%s)", esp_err_to_name(err));
          parser_get_context(parser)->err = "esp_ota_write failed";
          return 0;
        }
      }
      else
      {
        parser_get_context(parser)->err = "OTA boot partition not selected";
        ESP_LOGE(TAG, "%s", parser_get_context(parser)->err);
        return 0;
      }
    }
  }
  return 0;
}

/*---------------------------------------------------------------------------*/

static int on_part_end(multipartparser* parser)
{
  ESP_LOGD(TAG, "on_part_end");
  parser_get_context(parser)->state = P_STATE_INIT;
  return 0;
}

/*---------------------------------------------------------------------------*/

static void reset_timer_cb(TimerHandle_t pxTimer)
{
  ESP_LOGW(TAG, "Restatring...");
  esp_restart();
}

/*****************************************************************************/

static esp_err_t index_html_handler(httpd_req_t *req)
{
  ESP_LOGI(TAG, "index.html requested");
  httpd_resp_set_type(req, "text/html");
  httpd_resp_send(req, (const char *)index_html_start, index_html_end - index_html_start);
  return ESP_OK;
}

static esp_err_t favicon_handler(httpd_req_t *req)
{
  ESP_LOGI(TAG, "favicon.png requested");
  httpd_resp_set_type(req, "image/png");
  httpd_resp_send(req, (const char *)favicon_png_start, favicon_png_end - favicon_png_start);
  return ESP_OK;
}

static esp_err_t upload_post_handler(httpd_req_t *req)
{
  char buf[512];
  struct parser_context context = {
    .state = P_STATE_INIT
  };
  
  if(httpd_req_get_hdr_value_str(req, "Content-Type", buf, sizeof buf) != ESP_OK)
  {
    context.err = "Can not get Content-Type";
    goto end;
  }

  ESP_LOGI(TAG, "Header content: %s", buf);

  char* boundary = lwip_strnstr(buf, "boundary=", sizeof buf);
  if(boundary == NULL)
  {
    context.err = "Can not get boundary";
    goto end;
  }
  boundary += 9;

  ESP_LOGD(TAG, "Boundary: %s", boundary);

  multipartparser_callbacks callbacks = {
    .on_part_begin = on_part_begin,
    .on_header_field = on_header_field,
    .on_header_value = on_header_value,
    .on_data = on_data,
    .on_part_end = on_part_end,
  };
  multipartparser parser;
  multipartparser_init(&parser, boundary);
  parser_set_context(&parser, &context);

  int ret, remaining = req->content_len;
  while(remaining > 0)
  {
    /* Read the data for the request */
    if((ret = httpd_req_recv(req, buf, MIN(remaining, sizeof buf))) <= 0)
    {
      /* Retry receiving if timeout occurred */
      if (ret == HTTPD_SOCK_ERR_TIMEOUT)
        continue;

      return ESP_FAIL;
    }
    remaining -= ret;

    if(multipartparser_execute(&parser, &callbacks, buf, ret) != ret)
    {
      ESP_LOGE(TAG, "Multi part parse error");
      goto end;
    }

    if(context.err)
      goto end;
  }

  if(context.update_handle)
  {
    esp_err_t err = esp_ota_end(context.update_handle);
    if(err != ESP_OK)
    {
      if (err == ESP_ERR_OTA_VALIDATE_FAILED)
      {
        context.err = "Image validation failed, image is corrupted";
        ESP_LOGE(TAG, "%s", context.err);
      }
      ESP_LOGE(TAG, "esp_ota_end failed (%s)!", esp_err_to_name(err));
    }
  }

  if(context.partition)
  {
    esp_err_t err = esp_ota_set_boot_partition(context.partition);
    if(err != ESP_OK)
    {
      ESP_LOGE(TAG, "esp_ota_set_boot_partition failed (%s)!", esp_err_to_name(err));
      context.err = "Set boot partition failed";
      goto end;
    }
    /* update successed */
    httpd_resp_set_type(req, "text/html");
    httpd_resp_send(req, (const char *)updated_html_start, updated_html_end - updated_html_start);
    httpd_unregister_uri(req->handle, "/upload");

    ESP_LOGW(TAG, "Prepare to restart system after 10 seconds!");
    xTimerStart(xTimerCreate("RestartTimer", pdMS_TO_TICKS(10000), pdTRUE, 0, reset_timer_cb), portMAX_DELAY);
    return ESP_OK;
  }
  else
  {
    context.err = "What should i do ?";
    goto end;
  }

end:
  if(context.err)
  {
    ESP_LOGE(TAG, "upload_post_handler(): ERROR");

    char *buf = NULL;
    int len = asprintf(&buf, (const char*)error_html_start, context.err);

    if(buf)
    {
      httpd_resp_set_type(req, "text/html");
      httpd_resp_send(req, buf, len);
      free(buf);
    }
    else
    {
      ESP_LOGE(TAG, "asprintf allocate problem");
    }
  }
  return ESP_OK;
}

/*****************************************************************************/

esp_err_t swagger_json_handler(httpd_req_t *req)
{
  httpd_resp_set_type(req, "application/json");
  httpd_resp_send(req, (const char *)swagger_json_start,  swagger_json_end - swagger_json_start);
  return ESP_OK;
}

/*****************************************************************************/

esp_err_t swagger_html_handler(httpd_req_t *req)
{
  httpd_resp_set_hdr(req, "Content-Encoding", "gzip");
  httpd_resp_send(req, (const char *)swagger_html_gz_start, swagger_html_gz_end - swagger_html_gz_start);
  return ESP_OK;
}

/*****************************************************************************/

static esp_err_t api_ctrl_put_handler(httpd_req_t *req)
{
  httpd_resp_set_type(req, "application/json");
  cJSON *json = NULL;
  char *buf   = NULL;

  buf = malloc(req->content_len + 1);
  if(!buf)
    goto err;

  if(httpd_req_recv(req, buf, req->content_len) != req->content_len)
    goto err;

  buf[req->content_len] = 0;

  json = cJSON_Parse(buf);
  if(!json)
    goto err;


  cJSON *tmp = cJSON_GetObjectItem(json, "val");
  if (!tmp || !cJSON_IsNumber(tmp))
    goto err;

  uint32_t val = tmp->valueint;
  if (val > 100)
    goto err;

  void (*callback)(uint8_t) = req->user_ctx;
  callback(val);
  httpd_resp_set_status(req, HTTPD_200);
  goto end;

err:
  httpd_resp_set_status(req, HTTPD_400);

end:

  httpd_resp_send(req, NULL, 0);

  if(json)
  {
    cJSON_Delete(json);
  }
  if(buf)
  {
    free(buf);
  }

  return ESP_OK;
}

/*****************************************************************************/

static esp_err_t api_wifi_put_handler(httpd_req_t *req)
{
  httpd_resp_set_type(req, "application/json");
  cJSON *json = NULL;
  char *buf   = NULL;

  buf = malloc(req->content_len + 1);
  if(!buf)
    goto err;

  if(httpd_req_recv(req, buf, req->content_len) != req->content_len)
    goto err;

  buf[req->content_len] = 0;

  json = cJSON_Parse(buf);
  if(!json)
    goto err;

  uint8_t ssid[32];
  uint8_t pass[64];

  cJSON *tmp = cJSON_GetObjectItem(json, "ssid");
  if (!tmp || !cJSON_IsString(tmp))
    goto err;
  strlcpy((char*)ssid, (char*)tmp->valuestring, sizeof ssid);
  tmp = cJSON_GetObjectItem(json, "pass");
  if (!tmp || !cJSON_IsString(tmp))
    goto err;
  strlcpy((char*)pass, (char*)tmp->valuestring, sizeof pass);

  g_callback.on_set_wifi_credentials(ssid, pass);
  httpd_resp_set_status(req, HTTPD_200);
  goto end;

err:
  httpd_resp_set_status(req, HTTPD_400);

end:

  httpd_resp_send(req, NULL, 0);

  if(json)
  {
    cJSON_Delete(json);
  }
  if(buf)
  {
    free(buf);
  }

  return ESP_OK;
}

/*****************************************************************************/

void http_server_start(struct http_server_context *context)
{

  g_callback = *context;

  httpd_uri_t api_html =
  {
    .uri      = "/api",
    .method   = HTTP_GET,
    .handler  = swagger_html_handler,
    .user_ctx = NULL,
  };

  httpd_uri_t swagger_json =
  {
    .uri      = "/v1/swagger.json",
    .method   = HTTP_GET,
    .handler  = swagger_json_handler,
    .user_ctx = NULL,
  };

  httpd_uri_t api_brightness_json =
  {
    .uri       = "/v1/brightness",
    .method    = HTTP_PUT,
    .handler   = api_ctrl_put_handler,
    .user_ctx  = g_callback.on_set_brightness
  };

  httpd_uri_t api_balance_json =
  {
    .uri       = "/v1/balance",
    .method    = HTTP_PUT,
    .handler   = api_ctrl_put_handler,
    .user_ctx  = g_callback.on_set_balance
  };

  httpd_uri_t api_wifi_json =
  {
    .uri       = "/v1/wifi",
    .method    = HTTP_PUT,
    .handler   = api_wifi_put_handler,
    .user_ctx  = NULL
  };

  httpd_uri_t index_html =
  {
    .uri = "/",
    .method = HTTP_GET,
    .handler = index_html_handler,
    .user_ctx = NULL
  };

  httpd_uri_t favicon =
  {
    .uri = "/assets/favicon.png",
    .method = HTTP_GET,
    .handler = favicon_handler,
    .user_ctx = NULL
  };

  httpd_uri_t upload =
  {
    .uri       = "/upload",
    .method    = HTTP_POST,
    .handler   = upload_post_handler,
    .user_ctx  = NULL
  };

  httpd_config_t config = HTTPD_DEFAULT_CONFIG();

  ESP_LOGI(TAG, "Starting server on port: '%d'", config.server_port);
  httpd_handle_t server = NULL;
  if(httpd_start(&server, &config) == ESP_OK)
  {
    ESP_LOGI(TAG, "Registering URI handlers");
    httpd_register_uri_handler(server, &index_html);
    httpd_register_uri_handler(server, &favicon);
    httpd_register_uri_handler(server, &upload);
    httpd_register_uri_handler(server, &api_html);
    httpd_register_uri_handler(server, &swagger_json);
    httpd_register_uri_handler(server, &api_brightness_json);
    httpd_register_uri_handler(server, &api_balance_json);
    httpd_register_uri_handler(server, &api_wifi_json);
  }
  else
  {
    ESP_LOGI(TAG, "Error starting server!");
  }
}

/*****************************************************************************/
