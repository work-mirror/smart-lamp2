#include "ota.h"
#include "esp_ota_ops.h"
#include "esp_log.h"

static const char *TAG = "ota";

/*****************************************************************************/

bool ota_need_verify(void)
{
    const esp_partition_t *running = esp_ota_get_running_partition();
    esp_ota_img_states_t ota_state;
    if (esp_ota_get_state_partition(running, &ota_state) == ESP_OK) {
        if (ota_state == ESP_OTA_IMG_PENDING_VERIFY)
          return true;
    }
    return false;
}

/*****************************************************************************/

esp_err_t ota_confirm(void)
{
    if (ota_need_verify()) {
        ESP_LOGI(TAG, "Diagnostics completed successfully! Continuing execution ...");
        esp_ota_mark_app_valid_cancel_rollback();
        return ESP_OK;
    }
    else
    {
      ESP_LOGE(TAG, "ota already confirmed");
      return ESP_FAIL;
    }
}

/*****************************************************************************/