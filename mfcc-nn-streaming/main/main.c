#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include "esp_log.h"
#include "board.h"
#include "audio_common.h"
#include "audio_pipeline.h"
#include "i2s_stream.h"
#include "raw_stream.h"
#include "kws.h"
#include "rgbled.h"
#include "brightness.h"
#include "non_volatile_storage.h"
#include "server.h"
#include "esp_wifi.h"
#include "esp_event_loop.h"
#include "ota.h"
#include <sys/param.h>  // For MIN/MAX(a, b)

enum {
  RGB_GPIO_RED   = 21,
  RGB_GPIO_GREEN = 22,
  RGB_GPIO_BLUE  = 23,
};

enum
{
  BIT_KEYWORD = BIT0,
  BIT_SWITCH  = BIT1,
  BIT_BRIGHT  = BIT5,
  BIT_DARK    = BIT6,
  BIT_WHITE   = BIT7,
  BIT_YELLOW  = BIT8,
  BIT_UNKNOWN = BIT10,
  BIT_PUBLIC  = BIT11,
};

static xQueueHandle queue;
static EventGroupHandle_t event;

static const char  *TAG = "main";

/*****************************************************************************/

static const typeof(*((struct nvs_device_defaults *)0)->integers.arr) defaults_integers[] =
{
  { NVS_INT_USER_BUTTON_PRESS_COUNTER,  0     },  
  { NVS_INT_LAMP_BRIGHTNESS,            50    },
  { NVS_INT_LAMP_COLOR_TEMPERATURE,     0     },
  { NVS_INT_LAMP_ONOFF,                 true  },
};

/*****************************************************************************/

uint32_t device_inc_switch_on_count()
{
  uint32_t switch_on_count;
  switch_on_count = nvs_get_integer(NVS_INT_USER_BUTTON_PRESS_COUNTER) + 1;
  nvs_set_integer(NVS_INT_USER_BUTTON_PRESS_COUNTER, switch_on_count);
  return switch_on_count;
}

/*****************************************************************************/

static void kws_callback(int keyword)
{
    assert(xQueueSend(queue, &keyword, 0) == pdPASS);
}

/*****************************************************************************/

static int filter1(const int keyword)
{
    static uint8_t stat[CONFIG_KWS_GUESS_MODEL_OUT_NUM];
    assert(keyword < CONFIG_KWS_GUESS_MODEL_OUT_NUM);

    for(int i = 0; i < CONFIG_KWS_GUESS_MODEL_OUT_NUM; i++)
    {
        const uint8_t v = stat[i];
        if(i == keyword)
        {
            if(v < 15) stat[i]++;
        }
        else
        {
            if(v) stat[i]--;
        }
    }

    int idx = -1;
    for(int i = 0, max = 10; i < CONFIG_KWS_GUESS_MODEL_OUT_NUM; i++)
    {
        const uint8_t v = stat[i];
        if(v > max)
            max = v, idx = i;
    }

    return idx;
}

/*****************************************************************************/

static int filter2(const int keyword)
{
    static int candidate = -1;
    const int filtered = filter1(keyword);
    if(filtered == candidate)
        return candidate;
    candidate = filtered;
    return -1;
}

/*****************************************************************************/

static int filter3(const int keyword)
{
    static int candidate = -1;
    const int filtered = filter2(keyword);
    if(filtered != candidate)
    {
        candidate = filtered;
        return candidate;
    }
    return -1;
}

/*****************************************************************************/

static void filter_task(void *parameters)
{
    for(;;)
    {
        int keyword;
        xQueueReceive(queue, &keyword, portMAX_DELAY);
        if(filter3(keyword) > -1)
        {
            xEventGroupSetBits(event, 1 << keyword);
        }
    }
    vTaskDelete(NULL);
}

/*****************************************************************************/

static EventBits_t wait_bit_and_clear(EventBits_t b, TickType_t t)
{
  xEventGroupClearBits(event, b);
  return b & xEventGroupWaitBits(event, b, pdTRUE /* clear */, pdFALSE /* all */, t);
}

/*****************************************************************************/

static void leds_task(void *parameters)
{
    bool    state = nvs_get_integer(NVS_INT_LAMP_ONOFF);
    uint8_t temp  = nvs_get_integer(NVS_INT_LAMP_COLOR_TEMPERATURE);
    uint8_t brig  = nvs_get_integer(NVS_INT_LAMP_BRIGHTNESS);

    // apply state
    pwm_smooth_set_balance(temp);
    if(state)
    {
        pwm_smooth_set_brightness(brig);
    }
    rgb_set_total_brightness(state ? RGB_LED_MAX_TOTAL_BRIGHTNESS : 10);

    for(;;)
    {
        rgb_set_color(LED_COLOR_GREEN);

        wait_bit_and_clear(BIT_KEYWORD, portMAX_DELAY);

        rgb_set_color_blink(LED_COLOR_YELLOW);
        ESP_LOGW(TAG, "Keyword detected, waiting 3 sec for command...");

        if(wait_bit_and_clear(BIT_PUBLIC, pdMS_TO_TICKS(1000))) {

            const EventBits_t bits = wait_bit_and_clear(state
                                   ? BIT_SWITCH | BIT_WHITE | BIT_YELLOW | BIT_BRIGHT | BIT_DARK
                                   : BIT_SWITCH,
                                   pdMS_TO_TICKS(3000));

            if(bits && wait_bit_and_clear(BIT_PUBLIC, pdMS_TO_TICKS(1000)))
            {
                ESP_LOGD(TAG, "Memory Total:%d Bytes, SPI:%d Bytes, Inter:%d Bytes, Dram:%d Bytes",
                       esp_get_free_heap_size(),
                       heap_caps_get_free_size(MALLOC_CAP_SPIRAM),
                       heap_caps_get_free_size(MALLOC_CAP_INTERNAL),
                       heap_caps_get_free_size(MALLOC_CAP_INTERNAL | MALLOC_CAP_8BIT));

                if(bits & BIT_SWITCH)
                {
                    ESP_LOGI(TAG, "Switch");
                    state = !state;
                    pwm_smooth_set_brightness(state ? brig : 0);
                    rgb_set_total_brightness(state ? RGB_LED_MAX_TOTAL_BRIGHTNESS : 10);
                    ESP_LOGI(TAG, "State %s to nvs", state ? "on" : "off");
                    nvs_set_integer(NVS_INT_LAMP_ONOFF, state);
                }
                else if(bits & BIT_WHITE || bits & BIT_YELLOW)
                {
                    ESP_LOGI(TAG, "%s", bits & BIT_WHITE ? "White" : "Yellow");
                    uint8_t t = bits & BIT_WHITE ? 100 : 0;
                    if(temp != t)
                    {
                        pwm_smooth_set_balance(temp = t);
                        ESP_LOGI(TAG, "Temp %d to nvs", temp);
                        nvs_set_integer(NVS_INT_LAMP_COLOR_TEMPERATURE, temp);
                    }
                }
                else if(bits & BIT_BRIGHT || bits & BIT_DARK)
                {
                    ESP_LOGI(TAG, "%s", bits & BIT_BRIGHT ? "Bright" : "Dark");
                    uint8_t b = bits & BIT_BRIGHT
                                    ? MIN(brig + 25, 100)
                                    : MAX((int)brig - 25, 1);
                    if(brig != b)
                    {
                        pwm_smooth_set_brightness(brig = b);
                        ESP_LOGI(TAG, "Brig %d to nvs", brig);
                        nvs_set_integer(NVS_INT_LAMP_BRIGHTNESS, brig);
                    }
                }
            }
        }
    }

    vTaskDelete(NULL);
}

/*****************************************************************************/

static void audio_task(void *parameters)
{
    ESP_LOGI(TAG, "[ 1 ] Start codec chip");
    audio_board_handle_t board_handle = audio_board_init();
    audio_hal_ctrl_codec(board_handle->audio_hal, AUDIO_HAL_CODEC_MODE_BOTH, AUDIO_HAL_CTRL_START);

    audio_pipeline_handle_t pipeline;
    audio_element_handle_t i2s_stream_reader, filter, raw_read;

    ESP_LOGI(TAG, "[ 2.0 ] Create audio pipeline for recording");
    audio_pipeline_cfg_t pipeline_cfg = DEFAULT_AUDIO_PIPELINE_CONFIG();
    pipeline = audio_pipeline_init(&pipeline_cfg);
    mem_assert(pipeline);

    ESP_LOGI(TAG, "[ 2.1 ] Create i2s stream to read audio data from codec chip");
    i2s_stream_cfg_t i2s_cfg = I2S_STREAM_CFG_DEFAULT();
    i2s_cfg.type = AUDIO_STREAM_READER;
    i2s_cfg.i2s_config.sample_rate = 16000;
    i2s_cfg.i2s_config.channel_format = I2S_CHANNEL_FMT_ONLY_LEFT;
    i2s_cfg.i2s_config.mode &= ~I2S_MODE_TX;
    i2s_stream_reader = i2s_stream_init(&i2s_cfg);

    ESP_LOGI(TAG, "[ 2.3 ] Create raw to receive data");
    raw_stream_cfg_t raw_cfg = {
        .type = AUDIO_STREAM_READER,
    };
    raw_read = raw_stream_init(&raw_cfg);

    ESP_LOGI(TAG, "[ 3 ] Register all elements to audio pipeline");
    audio_pipeline_register(pipeline, i2s_stream_reader, "i2s");
    audio_pipeline_register(pipeline, raw_read, "raw");

    ESP_LOGI(TAG, "[ 4 ] Link elements together [codec_chip]-->i2s_stream-->raw-->[SR]");
    audio_pipeline_link(pipeline, (const char *[]) {"i2s",  "raw"}, 2);

    ESP_LOGI(TAG, "[ 5 ] Start audio_pipeline");
    audio_pipeline_run(pipeline);

    audio_element_info_t i2s_info = {0};
    audio_element_getinfo(i2s_stream_reader, &i2s_info);

    ESP_LOGI(TAG, "Initialize KWS");
    const size_t read_buf_sz = 3200;
    void* const buf = kws_init(i2s_info.sample_rates, i2s_info.channels,
                               i2s_info.bits, read_buf_sz,
                               kws_callback);

    ESP_LOGW(TAG, "Memory Total:%d Bytes, SPI:%d Bytes, Inter:%d Bytes, Dram:%d Bytes",
             esp_get_free_heap_size(),
             heap_caps_get_free_size(MALLOC_CAP_SPIRAM),
             heap_caps_get_free_size(MALLOC_CAP_INTERNAL),
             heap_caps_get_free_size(MALLOC_CAP_INTERNAL | MALLOC_CAP_8BIT));

    for(;;) {
        raw_stream_read(raw_read, buf, read_buf_sz);
        kws_detect();
    }

    ESP_LOGI(TAG, "[ 6 ] Stop audio_pipeline");

    audio_pipeline_terminate(pipeline);

    /* Terminate the pipeline before removing the listener */
    audio_pipeline_remove_listener(pipeline);

    audio_pipeline_unregister(pipeline, raw_read);
    audio_pipeline_unregister(pipeline, i2s_stream_reader);
    audio_pipeline_unregister(pipeline, filter);

    /* Release all resources */
    audio_pipeline_deinit(pipeline);
    audio_element_deinit(raw_read);
    audio_element_deinit(i2s_stream_reader);
    audio_element_deinit(filter);

    vTaskDelete(NULL);
}

/*****************************************************************************/

static void ota_tmr(TimerHandle_t pTimer)
{
    ESP_LOGW(TAG, "Restatring...");
    esp_restart();
}

/*****************************************************************************/

static esp_err_t event_handler(void *ctx, system_event_t *event)
{
    switch(event->event_id) {
    case SYSTEM_EVENT_STA_START:
        ESP_LOGI(TAG, "STA started !");
        esp_wifi_connect();
        break;
    case SYSTEM_EVENT_STA_GOT_IP:
        ESP_LOGI(TAG, "STA got ip:%s",
                 ip4addr_ntoa(&event->event_info.got_ip.ip_info.ip));
        break;
    case SYSTEM_EVENT_STA_DISCONNECTED:
        ESP_LOGI(TAG,"STA retry to connect to the AP");
        esp_wifi_connect();
        break;
    case SYSTEM_EVENT_AP_STACONNECTED:
        ESP_LOGI(TAG, "AP station:"MACSTR" join, AID=%d",
                 MAC2STR(event->event_info.sta_connected.mac),
                 event->event_info.sta_connected.aid);
        if(ctx && ota_confirm() == ESP_OK)
        {
            xTimerStart(xTimerCreate("ota", pdMS_TO_TICKS(9999), pdFALSE, NULL, ota_tmr), portMAX_DELAY);
        }
        break;
    case SYSTEM_EVENT_AP_STADISCONNECTED:
        ESP_LOGI(TAG, "AP station:"MACSTR"leave, AID=%d",
                 MAC2STR(event->event_info.sta_disconnected.mac),
                 event->event_info.sta_disconnected.aid);
        break;
    default:
        break;
    }
    return ESP_OK;
}

/*****************************************************************************/

static void wifi_init_softap(bool ota_need_verify)
{
    tcpip_adapter_init();
    ESP_ERROR_CHECK(esp_event_loop_init(event_handler, (void*)ota_need_verify));

    wifi_init_config_t cfg = WIFI_INIT_CONFIG_DEFAULT();
    ESP_ERROR_CHECK(esp_wifi_init(&cfg));
    wifi_config_t wifi_config = {
        .ap = {
            .max_connection = 5,
            .authmode = WIFI_AUTH_OPEN
        },
    };

    uint8_t mac[6];
    ESP_ERROR_CHECK(esp_wifi_get_mac(ESP_IF_WIFI_AP, mac));
    assert(sizeof wifi_config.ap.ssid > snprintf((char *)wifi_config.ap.ssid, 
        sizeof wifi_config.ap.ssid, "LAMP_%02X%02X", mac[4], mac[5]));

    ESP_ERROR_CHECK(esp_wifi_set_mode(WIFI_MODE_AP));
    ESP_ERROR_CHECK(esp_wifi_set_config(ESP_IF_WIFI_AP, &wifi_config));
    ESP_ERROR_CHECK(esp_wifi_start());
}

/*****************************************************************************/

static void wifi_credentials_task(void *parameters)
{
    vTaskDelay(pdMS_TO_TICKS(999));
    wifi_config_t *sta_config = parameters;
    ESP_LOGI(TAG,"Wi-Fi sta timer ssid:%s pass:%s",
             sta_config->sta.ssid, sta_config->sta.password);
    ESP_ERROR_CHECK(esp_wifi_stop());
    ESP_ERROR_CHECK(esp_wifi_set_storage(WIFI_STORAGE_RAM));
    ESP_ERROR_CHECK(esp_wifi_set_mode(WIFI_MODE_APSTA));
    ESP_ERROR_CHECK(esp_wifi_set_config(WIFI_IF_STA, sta_config));
    ESP_ERROR_CHECK(esp_wifi_start());
    free(sta_config);
    vTaskDelete(NULL);
}

/*****************************************************************************/

static void on_wifi_credentials(uint8_t* ssid, uint8_t* pass)
{
    wifi_config_t sta_config = {};
    strlcpy((char*)sta_config.sta.ssid, (char*)ssid, sizeof(sta_config.sta.ssid));
    strlcpy((char*)sta_config.sta.password, (char*)pass, sizeof(sta_config.sta.password));
    void *heap = malloc(sizeof sta_config);
    assert(heap);
    memcpy(heap, &sta_config, sizeof sta_config);
    assert(xTaskCreate(&wifi_credentials_task, "wifi_credentials_task", 3072, heap, 1, NULL) == pdPASS);
}

/*****************************************************************************/

static void clr_cnt_task(void *parameters)
{
    vTaskDelay(pdMS_TO_TICKS(9999));
    ESP_LOGI(TAG,"Reset user button counter from timer");
    nvs_set_integer(NVS_INT_USER_BUTTON_PRESS_COUNTER, 0);
    vTaskDelete(NULL);
}

/*****************************************************************************/

void app_main()
{
    esp_log_level_set("*", ESP_LOG_INFO);

    const struct nvs_device_defaults nvs_device_defaults =
    {
      .integers = {
        .arr    = defaults_integers,
        .length = sizeof defaults_integers / sizeof *defaults_integers
      }
    };

    nvs_init(&nvs_device_defaults);

    rgb_initialize((rgb_gpio_t){RGB_GPIO_RED, RGB_GPIO_GREEN, RGB_GPIO_BLUE, RGB_LED_COMMON_PIN_VDD},
                   (rgb_channel_t){LEDC_CHANNEL_0,  LEDC_CHANNEL_1, LEDC_CHANNEL_2}, 100);

    pwm_smooth_create_task("brighness", 3072, 1);

    struct http_server_context http_server_context =
    {
        .on_set_brightness       = pwm_smooth_set_brightness,
        .on_set_balance          = pwm_smooth_set_balance,
        .on_set_wifi_credentials = on_wifi_credentials
    };

    if(ota_need_verify())
    {
        rgb_set_color_blink(LED_COLOR_BLUE);
        wifi_init_softap(true);
        http_server_start(&http_server_context);
    }
    else if(device_inc_switch_on_count() > 3)
    {
        nvs_set_integer(NVS_INT_USER_BUTTON_PRESS_COUNTER, 0);
        rgb_set_color(LED_COLOR_BLUE);
        wifi_init_softap(false);
        http_server_start(&http_server_context);
    }
    else
    {
        assert(xTaskCreate(&clr_cnt_task, "clr_cnt_task", 3072, NULL, 1, NULL) == pdPASS);
        assert(queue = xQueueCreate(5, sizeof(int)));
        assert(event = xEventGroupCreate());
        assert(xTaskCreate(&leds_task, "leds_task", 3072, NULL, 1, NULL) == pdPASS);
        assert(xTaskCreate(&filter_task, "filter_task", 3072, NULL, 1, NULL) == pdPASS);
        assert(xTaskCreate(&audio_task, "main", 3072, NULL, 2, NULL) == pdPASS);
    }
}

/*****************************************************************************/
