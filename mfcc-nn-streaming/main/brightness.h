/******************************************************************************
 * @copyright Copyright (c) A1 Company LLC. All rights reserved.
 *****************************************************************************/

#pragma once /****************************************************************/

#include <stdint.h>

void pwm_smooth_create_task(const char *const name,
                            uint32_t depth,
                            UBaseType_t priority);

void pwm_smooth_set_brightness(uint8_t value);

void pwm_smooth_set_balance(uint8_t value);

void pwm_smooth_set_on_off(bool on, uint8_t brightness, uint8_t balance, uint8_t seconds);

void pwm_smooth_fade_on_off(bool on, uint8_t brightness, uint8_t balance, uint16_t minutes);

void pwm_smooth_set_smooth_onoff_time(uint8_t value);

/*****************************************************************************/