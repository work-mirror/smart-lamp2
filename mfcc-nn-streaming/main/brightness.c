/******************************************************************************
 * @copyright Copyright (c) A1 Company LLC. All rights reserved.
 *****************************************************************************/

#include "freertos/FreeRTOS.h"
#include "freertos/task.h"
#include "freertos/queue.h"
#include "driver/ledc.h"
#include "esp_log.h"
#include "brightness.h"

#define LED_BRIGHTNESS_GPIO                                                  18
#define LED_BRIGHTNESS_CHANNEL                                                3

#define LED_BALANCE_GPIO                                                     19
#define LED_BALANCE_CHANNEL                                                   4

#define LED_ONOFF_GPIO                                                       27
#define LED_MD_ONOFF_GPIO                                                    33

#define PWM_MAX  (4096 - 1 /* 12 bit */)
#define MIN_BR   (47)
#define MAX_BR   (100 + MIN_BR)

static const char *TAG = "pwm_smooth";
static QueueHandle_t queue;

typedef struct
{
  uint8_t value;
  TickType_t ticks;
}
queue_item_t;

/*****************************************************************************/

static void set_brightness(uint8_t v)
{
  // 4095.0 / (147 ** 3) = 0.0012891453957676365
  const uint32_t d = v == MAX_BR ? PWM_MAX : 0.0012891453957676365 * v * v * v;
  assert(d <= PWM_MAX);
  ledc_set_duty(LEDC_HIGH_SPEED_MODE, LED_BRIGHTNESS_CHANNEL, d);
  ledc_update_duty(LEDC_HIGH_SPEED_MODE, LED_BRIGHTNESS_CHANNEL);
  gpio_set_level(LED_ONOFF_GPIO, !!v);
}

/*****************************************************************************/

static void pwm_smooth_task(void *parameters)
{
  struct
  {
    uint8_t curr, dest;
  } br = {0};
  bool active = false;
  TickType_t delay = 0;

  set_brightness(0); // cold start

  for(;;)
  {
    queue_item_t item;
    if(xQueueReceive(queue, &item, active ? 0 : portMAX_DELAY) == pdTRUE)
    {
      br.dest = item.value;
      delay = item.ticks;
    }

    // printf("cur:%d dest:%d del:%d\n", br.curr, br.dest, delay);

    active = false;
    if(br.dest != br.curr)
    {
      active = true;
      br.curr = delay ? br.dest > br.curr ? br.curr + 1 : br.curr - 1 : br.dest;
      set_brightness(br.curr);
      vTaskDelay(delay);
    }
  }
  vTaskDelete(NULL);
}

/*****************************************************************************/

static void queue_add_brightness(uint8_t value, TickType_t step)
{
  if(value > 100)
  {
    ESP_LOGE(TAG, "brigh out of bounds %d", value);
  }
  else
  {
    value = value ? value + MIN_BR : 0;
    assert(value <= MAX_BR);
    queue_item_t item =
    {
      .value = value,
      .ticks = step,
    };
    xQueueOverwrite(queue, &item);
  }
}

/*****************************************************************************/

void pwm_smooth_create_task(const char *const name,
                            uint32_t depth,
                            UBaseType_t priority)
{
  ledc_channel_config_t ledc_channel_balance =
  {
    .gpio_num = LED_BALANCE_GPIO,
    .speed_mode = LEDC_HIGH_SPEED_MODE,
    .channel = LED_BALANCE_CHANNEL,
    .intr_type = LEDC_INTR_FADE_END,
    .timer_sel = LEDC_TIMER_2,
    .duty = 0,
  };
  ESP_ERROR_CHECK(ledc_channel_config(&ledc_channel_balance));

  ledc_timer_config_t ledc_timer_balance =
  {
    .speed_mode = LEDC_HIGH_SPEED_MODE,
    .bit_num = LEDC_TIMER_12_BIT,
    .timer_num = LEDC_TIMER_2,
    .freq_hz = 19500,
  };
  ESP_ERROR_CHECK(ledc_timer_config(&ledc_timer_balance));

  ledc_channel_config_t ledc_channel_bright =
  {
    .gpio_num = LED_BRIGHTNESS_GPIO,
    .speed_mode = LEDC_HIGH_SPEED_MODE,
    .channel = LED_BRIGHTNESS_CHANNEL,
    .intr_type = LEDC_INTR_FADE_END,
    .timer_sel = LEDC_TIMER_1,
    .duty = 0,
  };
  ESP_ERROR_CHECK(ledc_channel_config(&ledc_channel_bright));

  ledc_timer_config_t ledc_timer_bright =
  {
    .speed_mode = LEDC_HIGH_SPEED_MODE,
    .bit_num = LEDC_TIMER_12_BIT,
    .timer_num = LEDC_TIMER_1,
    .freq_hz = 19500,
  };
  ESP_ERROR_CHECK(ledc_timer_config(&ledc_timer_bright));

  ledc_fade_func_install(0);

  ledc_set_duty(LEDC_HIGH_SPEED_MODE, LED_BALANCE_CHANNEL, 0);
  ledc_update_duty(LEDC_HIGH_SPEED_MODE, LED_BALANCE_CHANNEL);

  ledc_set_duty(LEDC_HIGH_SPEED_MODE, LED_BRIGHTNESS_CHANNEL, 0);
  ledc_update_duty(LEDC_HIGH_SPEED_MODE, LED_BRIGHTNESS_CHANNEL);

  gpio_pad_select_gpio(LED_ONOFF_GPIO);
  gpio_set_direction(LED_ONOFF_GPIO, GPIO_MODE_OUTPUT);
  gpio_set_level(LED_ONOFF_GPIO, 1);            // on power LED driver

  gpio_pad_select_gpio(LED_MD_ONOFF_GPIO);
  gpio_set_direction(LED_MD_ONOFF_GPIO, GPIO_MODE_OUTPUT);
  gpio_set_level(LED_MD_ONOFF_GPIO, 1);            // on power LED driver

  
  assert(queue = xQueueCreate(1, sizeof(queue_item_t)));
  xTaskCreate(&pwm_smooth_task, name, depth, NULL, priority, NULL);
}

/*****************************************************************************/

void pwm_smooth_set_brightness(uint8_t value)
{
  queue_add_brightness(value, pdMS_TO_TICKS(20));
}

/*****************************************************************************/

void pwm_smooth_set_on_off(bool on, uint8_t brightness, uint8_t balance, uint8_t seconds)
{
  if(brightness)
  {
    _Static_assert(portTICK_PERIOD_MS == 10, "maybe we don't need +1");
    TickType_t step = seconds ? pdMS_TO_TICKS(1000L * seconds / (brightness + MIN_BR)) + 1 : 0;
    if(step > 5) step = 5; // brightness flicking workaround
    queue_add_brightness(on ? brightness : 0, step);
    pwm_smooth_set_balance(balance);
  }
  else
  {
    ESP_LOGE(TAG, "brigh out of bounds %d", brightness);
  }
}

/*****************************************************************************/

void pwm_smooth_fade_on_off(bool on, uint8_t brightness, uint8_t balance, uint16_t minutes)
{
  if(brightness)
  {
    if(minutes)
    {
      TickType_t step = pdMS_TO_TICKS(60L * 1000L * minutes / (brightness + MIN_BR));
      queue_add_brightness(on ? brightness : 0, step);
      pwm_smooth_set_balance(balance);
    }
    else
    {
      ESP_LOGE(TAG, "minutes out of bounds %d", minutes);
    }
  }
  else
  {
    ESP_LOGE(TAG, "brigh out of bounds %d", brightness);
  }
}

/*****************************************************************************/

void pwm_smooth_set_balance(uint8_t value)
{
  if(value > 100)
  {
    ESP_LOGE(TAG, "balance out of bounds %d", value);
  }
  else
  {
    ledc_set_duty(LEDC_HIGH_SPEED_MODE, LED_BALANCE_CHANNEL, ((uint32_t)PWM_MAX) * value / 100);
    ledc_update_duty(LEDC_HIGH_SPEED_MODE, LED_BALANCE_CHANNEL);
  }
}

/*****************************************************************************/